"use strict";

$('.slider').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    arrows: false
});

new WOW().init();
/*$(document).ready(function() {
  $('.main-content_headers').addClass('wow bounceInDown');
  $('.content_phone').addClass('wow bounceInDown');
})*/

$('.btn').click(function(){
    $('html, body').animate({scrollTop:$($(this).attr("href")).position().top}, 1500);
})
